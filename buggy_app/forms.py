from django import forms
from django.contrib.auth.models import User

from buggy_app.models import Booking,UserProfileInfo
from django.forms import ModelForm
from django.forms import extras
from datetimewidget.widgets import TimeWidget
from django.forms.extras.widgets import SelectDateWidget
from django import forms
from datetimepicker.helpers import js_loader_url
from bootstrap_datepicker_plus import TimePickerInput

import datetime




class BookingForm(ModelForm):

    class Meta:
        model = Booking
        widgets = {
            'times_pick': TimePickerInput(),

        }
        fields = ('first_name','last_name','email','book_car','contact_number','times_pick',)



class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta():
        model = User
        fields = ('username','email','password')


class UserProfileInfoForm(forms.ModelForm):
    class Meta():
        model = UserProfileInfo
        fields = ('portfolio_site','profile_pic')
