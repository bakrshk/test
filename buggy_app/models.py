from django.db import models
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.text import slugify
from datetime import date
import uuid


class UserProfileInfo(models.Model):

    # Create relationship (don't inherit from User!)
    user = models.OneToOneField(User)

    portfolio_site = models.URLField(blank=True)
    profile_pic = models.ImageField(upload_to='profile_pics',blank=True)

    def __str__(self):
        return self.user.username


class Color(models.Model):
    name = models.CharField(max_length=16)
    hex = models.CharField(max_length=16)
    def __str__(self):
        return self.name

class Brand(models.Model):
    name = models.CharField(max_length=16)
    def __str__(self):
        return self.name


class Times(models.Model):
    time_type = models.CharField(max_length = 30)
    timeslot= models.TimeField(blank = True)
    time_check= models.BooleanField(default = True)

    def __str__(self):
        return self.time_type




class Car(models.Model):
    TRANSMISSION_CHOICES = (
        ('Auto', 'Auto'),
        ('Manual', 'Manual')
    )
    SEATS_CHOICES = (
        ('2', '2'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        )
    FUEL_CHECK = (
        ('Petrol', 'PETROL'),
        ('Diesel', 'Diesel'),
        ('Gas', 'Gas'),
    )
    reg_no = models.CharField(max_length=16, unique=True)
    model_year = models.DateField()
    car_name =models.CharField(max_length=40)
    color = models.ForeignKey(Color,on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand,on_delete=models.CASCADE)
    times_for = models.ForeignKey(Times,on_delete=models.CASCADE)

    car_image = models.ImageField(upload_to='cars', blank = True)
    description = models.CharField(max_length=200)
    is_available_car = models.BooleanField(default = False)
    car_price = models.IntegerField(blank = False)
    power_hp = models.CharField(max_length= 20 , blank = True)
    transmission = models.CharField(max_length = 10 , choices = TRANSMISSION_CHOICES)
    seats = models.CharField(max_length=10, choices = SEATS_CHOICES)
    air_bags = models.CharField (max_length = 10 , choices = SEATS_CHOICES)
    car_image_internal = models.ImageField(upload_to='cars', blank = True)
    car_image_internal2 = models.ImageField(upload_to='cars', blank = True)
    fuel_type = models.CharField(max_length= 20 , choices = FUEL_CHECK)

    def __str__(self):
        return self.car_name

    def get_absolute_url(self):
        return reverse("buggy_app:details",kwargs={'pk':self.pk})



class Customer(models.Model):
    first_name = models.CharField(max_length=64,null= False)
    last_name = models.CharField(max_length=64)
    customer_age = models.IntegerField(blank=True, null=True)
    last_name = models.CharField(blank=True, max_length=100)
    license_year = models.IntegerField(blank=False, null=False)
    tlc_number = models.IntegerField(blank=False ,null=False)
    contact_num =models.IntegerField(blank=False, null=False)
    contact_email = models.EmailField(blank =False)


    def __str__(self):
        return self.first_name


class Booking(models.Model):
    first_name = models.CharField(max_length=240,null= False)
    last_name = models.CharField(max_length=64)
    email = models.EmailField(blank=False,null =False)
    contact_number  = models.IntegerField(blank=False, null=False)


    book_car = models.ForeignKey(Car, on_delete=models.CASCADE ,related_name='book_car')
    booking_start_date = models.DateTimeField(auto_now_add=True ,blank=False)
    booking_end_date = models.DateTimeField(blank = True , null = True)
    rental_price = models.IntegerField(blank=True, null=True)
    times_pick = models.TimeField(blank = True)

    is_approved = models.BooleanField(default=False)
    def __str__(self):
        return self.first_name

    def get_absolute_url(self):
        return reverse("buggy_app:detail",kwargs={'pk':self.pk})
class Driver(models.Model):
	class Meta():
		db_table = "driver"
		verbose_name = "Driver"
		verbose_name_plural = "Drivers"
		ordering = ['driver_firstname', 'driver_lastname']
	def get_driver_image_path_primary(self, filename):
		path = ''.join([str('static/img/customer/') + str(self.id) + str('/license/primary/'), slugify(filename) + str('.jpg')])
		return path

	driver_firstname = models.CharField(
		max_length=64,
		blank=True,
		null=True
	)
	driver_lastname = models.CharField(
		max_length=64,
		blank=True,
		null=True
	)
	driver_date_added  = models.DateTimeField(
		auto_now_add=True,
	)
	driver_date_modified = models.DateTimeField(
		auto_now = True,
	)
	driver_stripe_id = models.CharField(
		max_length=64,
		blank=True,
		null=True

	)
	driver_email = models.EmailField(
		blank=True
	)
	driver_dmv_license = models.CharField(
		max_length=64,
		blank=True,
		null=True
	)
	driver_tlc_license = models.CharField(
		max_length=64,
		blank = True,
		null = True
	)
	driver_notes = models.TextField(
		max_length=240,
		blank = True,
		null = True
	)
	driver_dob = models.DateField(
		blank = True,
		null = True
	)

	driver_license_date = models.DateField(
		blank=True,
		null=True
	)
	driver_tlc_license_date = models.DateField(
		blank=True,
		null=True
	)
	driver_foreign_license_date = models.DateField(
		blank=True,
		null=True
	)

	driver_accidents = models.PositiveIntegerField(
		blank=True,
		null=True,
	)

	driver_points = models.PositiveIntegerField(
		blank=True,
		null=True,
	)
	driver_police_report = models.BooleanField(
		default=False
		)
	driver_ddc_uploaded = models.BooleanField(
		default=False
		)
	driver_poa_uploaded = models.BooleanField(
		default=False
		)
	driver_ssc_uploaded = models.BooleanField(
		default=False
		)

	driver_current_agreement_id = models.PositiveIntegerField(
		blank=True,
		null=True,
	)
	driver_city = models.CharField(
		max_length=64,
		blank = True,
		null = True
	)
	driver_state = models.CharField(
		max_length=64,
		blank = True,
		null = True
	)
	driver_street_address = models.CharField(
		max_length=64,
		blank = True,
		null = True
	)
	driver_state = models.CharField(
		max_length=64,
		blank = True,
		null = True
	)
	driver_state = models.CharField(
		max_length=8,
		blank = True,
		null = True
	)
	driver_cap_insurance_state = models.CharField(
		max_length=64,
		blank = True,
		null = True
	)
	driver_ddc_date = models.CharField(
		max_length=8,
		blank = True,
		null = True
	)
	driver_insurance_active = models.CharField(
		max_length=64,
		blank = True,
		null = True
	)
	driver_insurance_note = models.TextField(
		max_length=240,
		blank = True,
		null = True
	)
	driver_insurance_reason = models.TextField(
		max_length=240,
		blank = True,
		null = True
	)
	driver_insurance_status = models.CharField(
		max_length=64,
		blank = True,
		null = True
	)
	driver_insurance_date = models.DateField(
		blank=True,
		null=True
	)
	driver_link_mvr = models.URLField(
		blank=True
	)
	driver_ddc_pic = models.ImageField(
		upload_to=get_driver_image_path_primary,
	)

	driver_license_pic = models.ImageField(
	upload_to=get_driver_image_path_primary,
	)
	driver_ssc_pic = models.ImageField(
		upload_to=get_driver_image_path_primary,
	)
	driver_tlc_license_pic = models.ImageField(
		upload_to=get_driver_image_path_primary,
	)
	driver_preferred_lang = models.CharField(
		max_length=64,
		blank = True,
		null = True
	)
	driver_base_additional_charge = models.PositiveIntegerField(
		blank=True,
		null=True
	)

	driver_auto_charge_cc = models.BooleanField(
		default=True
	)
	driver_charge_uber = models.BooleanField(
		default=True
	)
	driver_paydown_amount = models.PositiveIntegerField(
		blank=True,
		null=True
	)
	driver_balance= models.DecimalField(
		decimal_places=3,
		max_digits=10,
		blank=True,
		null=True
	)
	driver_deposit = models.DecimalField(
		decimal_places=3,
		max_digits=10,
		blank=True,
		null=True
	)
	driver_latest_payments = models.DecimalField(
		decimal_places=3,
		max_digits=10,
		blank=True,
		null=True
	)
	driver_billing_notes = models.CharField(
		max_length=240,
		blank = True,
		null = True
	)
	driver_charge_day_week = models.PositiveIntegerField(
		blank=True,
		null=True
	)
	driver_weekly_billing_notes = models.TextField(
		max_length=240,
		blank = True,
		null = True
	)
	driver_needs_collection = models.BooleanField(
		default = True,
	)
	driver_charge_ccsurcharge = models.BooleanField(
		default = True,
	)
	driver_paydown_lefts = models.DecimalField(
		decimal_places=3,
		max_digits=10,
		blank=True,
		null=True
	)


	driver_token_id = models.UUIDField(
		default=uuid.uuid4,
		unique=True,
		blank=False,
		null=False,
	)

	driver_agree_terms = models.BooleanField(
		default = True,
	)

	def __str__(self):
		return self.driver_firstname

class CarsApplication(models.Model):
	class Meta():
		db_table = "cars_application"
		verbose_name = "Cars Application"
		verbose_name_plural = "Cars Applications"

	cars_application_driver = models.ForeignKey(
		Driver,
		on_delete=models.CASCADE,
		related_name='cars_application_driver_key'
	)

	car_application_type_of_car_wanted = models.TextField(
		blank = True,
		null = True
	)
	car_application_policy_notes = models.TextField(
		blank = True,
		null = True
	)
	car_application_notes = models.TextField(
		blank = True,
		null = True
	)
	car_application_referral_id = models.CharField(
		max_length=240,
		blank = True,
		null = True
	)
	car_application_source_id = models.CharField(
		max_length=240,
		blank = True,
		null = True
	)
	car_application_promo_id = models.CharField(
		max_length=240,
		blank = True,
		null = True
	)
	car_application_worked_for_uber = models.BooleanField(
		default=True
	)
	car_application_driver_id = models.CharField(
		max_length=240,
		blank = True,
		null = True
	)
	car_application_represntive_id = models.PositiveIntegerField(
		blank=True,
		null=True
	)
	car_application_represntive_old_id = models.PositiveIntegerField(
		blank=True,
		null=True
	)
	car_application_stage_id = models.PositiveIntegerField(
		blank=True,
		null=True
	)
	car_application_checked_docs = models.BooleanField(
		default = False,
	)

	def __str__(self):
		return self.cars_application_driver.driver_firstname
