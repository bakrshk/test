from import_export import resources
from .models import Booking , Car

class BookingResource(resources.ModelResource):
    class Meta:
        model = Booking
class CarResource(resources.ModelResource):
    class Meta:
        model = Car
