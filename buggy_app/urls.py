from django.conf.urls import url
from . import views

app_name = 'buggy_app'

urlpatterns = [

    url(r'^carlist$',views.CarListView.as_view(),name='car_list'),
    url(r'^user_login/$',views.user_login,name='user_login'),
    url(r'^booklist$',views.BookingListView.as_view(),name='book_list'),

    url(r'^carlist/(?P<pk>\d+)/$',views.CarDetailView.as_view(),name='details'),
    url(r'^booking/',views.BookingView.as_view(),name='car_booking'),
    url(r'^booklist/(?P<pk>\d+)/$',views.BookingDetailView.as_view(),name='detail'),
    url(r'^create/$',views.BookingCreateView.as_view(),name='create'),
    url(r'^update/(?P<pk>\d+)/$',views.BookingUpdateView.as_view(),name='update'),
    url(r'^home/$',views.HomeView.as_view(),name='home'),
    url(r'^services/$',views.ServicesView.as_view(),name='services'),
    url(r'^export/xls/$', views.export_booking_xls, name='export_booking_xls'),
    url(r'^export/xls/car/$', views.export_car_xls, name='export_car_xls'),
    url(r'^user_login/$',views.user_login,name='user_login'),
    url(r'^pop/$',views.PopView.as_view(),name='pop'),
    url(r'^create_driver/$',views.DriverCreateView.as_view(),name='create_driver'),
    url(r'^driverlist/(?P<pk>\d+)/$',views.DriverDetailView.as_view(),name='driverdetail'),
    url(r'^driverlist$',views.DriverListView.as_view(),name='driver_list'),
    url(r'^dupdate/(?P<pk>\d+)/$',views.DriverUpdateView.as_view(),name='dupdate'),

]
