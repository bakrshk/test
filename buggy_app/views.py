from django.shortcuts import render ,redirect , get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.views.generic import (View,TemplateView,
                                ListView,DetailView,
                                CreateView,DeleteView,
                                UpdateView)

from .  import models
from buggy_app.models import Booking , Car
from buggy_app.forms import BookingForm , UserForm ,UserProfileInfoForm

from django.views.generic.edit import FormView
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import xlwt

from django.utils import timezone
import pytz
from django.template.loader import get_template
from django.conf import settings
from django.core.mail import  send_mail





class IndexView(TemplateView):

    template_name = 'buggy_app/index.html'

class HomeView(TemplateView):
    template_name = 'buggy_app/home.html'

class ServicesView(TemplateView):
    template_name = 'buggy_app/services.html'

class BookingListView(ListView):
    context_object_name = 'books'
    model = models.Booking


class BookingDetailView(DetailView):
    context_object_name = 'booking_details'
    model = models.Booking
    template_name = 'buggy_app/booking_detail.html'


class PopView(TemplateView):

    template_name = 'buggy_app/pop.html'



def detail(request, blog_id):
	detailblog = get_object_or_404(Blog, pk=blog_id)
	return render(request, 'blog/detail.html', {'blog':detailblog})


class BookingCreateView(CreateView):
    fields = ("customer_name","book_car","booking_end_date","rental_price","is_approved")
    model = models.Booking


class BookingUpdateView(UpdateView):
    fields = ("customer_name","book_car","booking_end_date","rental_price","is_approved")
    model = models.Booking


@login_required
def special(request):
    # Remember to also set login url in settings.py!
    # LOGIN_URL = '/basic_app/user_login/'
    return HttpResponse("You are logged in. Nice!")

@login_required
def user_logout(request):
    # Log out the user.
    logout(request)
    # Return to homepage.
    return HttpResponseRedirect(reverse('buggy_app:home'))

def user_login(request):

    if request.method == 'POST':
        # First get the username and password supplied
        username = request.POST.get('username')
        password = request.POST.get('password')

        # Django's built-in authentication function:
        user = authenticate(username=username, password=password)

        # If we have a user
        if user:
            #Check it the account is active
            if user.is_active:
                # Log the user in.
                login(request,user)
                # Send the user back to some page.
                # In this case their homepage.
                return HttpResponseRedirect(reverse('buggy_app:home'))
            else:
                # If account is not active:
                return HttpResponse("Your account is not active.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username,password))
            return HttpResponse("Invalid login details supplied.")

    else:
        #Nothing has been provided for username or password.
        return render(request, 'buggy_app/login.html', {})


class CarListView(ListView):
    context_object_name = 'cars'
    model = models.Car

    def get_queryset(self):
        qs = super(CarListView, self).get_queryset()
        qs = qs.filter(is_available_car=True)
        return qs


class CarDetailView(DetailView):
    context_object_name = 'car_details'
    model = models.Car
    template_name = 'buggy_app/car_detail.html'


class BookingView(FormView):
    template_name = 'buggy_app/booking.html'
    form_class = BookingForm
    models = Booking
    def form_valid(self, form):
        car_id = self.request.GET.get('car', '')
        car = Car.objects.get(id=car_id)
        car.is_available_car = True
        car.save()
        form.save()
        subject = "Thnakyou for joining Joinbuggy"
        from_email = settings.EMAIL_HOST_USER
        to_email = form.cleaned_data.get('email')
        join_message = """Your booking is placed we will contact you later """
        send_mail(subject=subject,from_email=from_email,recipient_list=[to_email],message=join_message,fail_silently=False )


        return super(BookingView, self).form_valid(form)


    success_url = reverse_lazy('buggy_app:pop')

    def get_context_data(self, **kwargs):
        # kwargs['car'] is the car booking now!
        try:
            kwargs['car'] = Car.objects.get(id=self.request.GET.get('car', ''))
        except (Car.DoesNotExist, ValueError):
            kwargs['car'] = None
        return super(BookingView, self).get_context_data(**kwargs)

    # if you still want to keep select car in form,and auto select this car do as:
    def get_initial(self):
        initial = super(BookingView, self).get_initial()
        if 'car' in self.request.GET:
            try:
                initial['book_car'] = Car.objects.get(id=self.request.GET['car'])
            except (Car.DoesNotExist, ValueError):
                pass
        return initial



def send_emails(first_name, last_name, to_email):
    #get template
    htmly = get_template('buggy_app/welcome.html')
    #create a context
    d = {'first_name':first_name, 'last_name':last_name}
    subject, from_email, to = 'Subject line', settings.EMAIL_HOST_USER, to_email
    #pass the context to html template
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
class DriverListView(ListView):
    model = models.Driver
    context_object_name = 'driver'


    def get_queryset(self):
        qs = super(DriverListView, self).get_queryset()
        qs = qs.filter(driver_agree_terms=True)
        return qs


class DriverDetailView(DetailView):
    context_object_name = 'driver_details'
    model = models.Driver
    template_name = 'buggy_app/driver_detail.html'
class DriverCreateView(CreateView):
    fields = ("driver_firstname","driver_lastname","driver_stripe_id","driver_email","driver_dmv_license","driver_tlc_license","driver_notes","driver_dob","driver_license_date","driver_tlc_license_date","driver_foreign_license_date","driver_accidents","driver_points","driver_police_report","driver_current_agreement_id","driver_city","driver_state","driver_street_address","driver_cap_insurance_state","driver_insurance_note","driver_link_mvr","driver_ddc_pic","driver_license_pic")
    model = models.Driver
class DriverUpdateView(UpdateView):
    fields = ("driver_firstname","driver_lastname","driver_stripe_id","driver_email","driver_dmv_license","driver_tlc_license","driver_notes","driver_dob","driver_license_date","driver_tlc_license_date","driver_foreign_license_date","driver_accidents","driver_points","driver_police_report","driver_current_agreement_id","driver_city","driver_state","driver_street_address","driver_cap_insurance_state","driver_insurance_note","driver_link_mvr","driver_ddc_pic","driver_license_pic")

    model = models.Driver
class BookingCreateView(CreateView):
    fields = ("customer_name","book_car","booking_end_date","rental_price","is_approved","times_pick")
    model = models.Booking


class BookingUpdateView(UpdateView):
    fields = ("first_name","book_car","rental_price","is_approved","times_pick")

    model = models.Booking

def export_booking_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="bookings.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Bookings')


    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Booking Name', 'Drivers', 'Cars Booked', 'Rental Price','Admin Approvel' ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = Booking.objects.all().values_list('booking_name', 'customer_name', 'book_car',  'rental_price', 'is_approved')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response
def export_car_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="cars.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Bookings')


    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['CAr Name', 'reg_no', 'description', 'power_hp','transmission','seats','air_bags ', 'fuel_type' ,'is_available_car' ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = Car.objects.all().values_list('car_name', 'reg_no', 'description',  'power_hp', 'transmission','seats','air_bags','fuel_type','is_available_car',)
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


'''
def booking_create(request,pk):
    book_car = get_object_or_404(Car, pk=pk)
    if request.method == "POST":
        form = BookingForm(request.POST)
        if form.is_valid():

            booking = form.save(commit=False)
            booking.book_car = book_car
            booking.save()
            return redirect('car_detail', pk=book_car.pk)
    else:
        form = BookingForm()
    return render(request, 'buggy_app/booking_form.html', {'form': form})
'''
